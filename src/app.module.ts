import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { MenuModule } from './menu/menu.module';
import { TableModule } from './table/table.module';
import { ReservationModule } from './reservation/reservation.module';
import { MulterModule } from '@nestjs/platform-express';
import { UploadModule } from './upload/upload.module';

@Module({
  imports: [
    UserModule,
    MenuModule,
    TableModule,
    ReservationModule,
    MulterModule.register({
      dest: './files',
    }),
    UploadModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
