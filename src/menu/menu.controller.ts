import { Controller, Get, Query, Post, Body } from '@nestjs/common';
import { ApiTags, ApiBearerAuth, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { MenuService } from './menu.service';
import { MenusRO } from './menu.interface';
import { Menu } from './menu.entity';
import { User } from '../user/user.decorator';
import { CreateMenuDto } from './dto/create-menu.dto';

@ApiBearerAuth()
@ApiTags('menu')
@Controller('menus')
export class MenuController {

  constructor(private readonly menuService: MenuService) {}

  @ApiOperation({ summary: 'Get all menus' })
  @ApiResponse({ status: 200, description: 'Return all menus.'})
  @Get()
  async findAll(@Query() query): Promise<Menu[]> {
    return await this.menuService.findAll();
  }

  @ApiOperation({ summary: 'Create menu' })
  @ApiResponse({ status: 201, description: 'New menu has been successfully created.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @Post()
  async create(@Body('menu') menuData: CreateMenuDto) {
    return this.menuService.create(menuData);
  }
}
