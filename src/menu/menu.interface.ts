export interface MenuData {
  id: number;
  title: string;
  description: string;
  price: number;
  image?: string;
}

export interface MenuRO {
  menu: MenuData;
}

export interface MenusRO {
  menus: MenuData[];
}