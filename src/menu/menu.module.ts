import { Module } from '@nestjs/common';
import { MenuService } from './menu.service';
import { MenuController } from './menu.controller';
import { menuProviders } from './menu.providers';
import { DatabaseModule } from 'src/db/database.module';

@Module({
  imports: [DatabaseModule],
  providers: [
    ...menuProviders,
    MenuService
  ],
  controllers: [MenuController],
  exports: [MenuService]
})
export class MenuModule {}
