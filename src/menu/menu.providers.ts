import { Connection } from 'typeorm';
import { Menu } from './menu.entity';

export const menuProviders = [
  {
    provide: 'MENU_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Menu),
    inject: ['DATABASE_CONNECTION'],
  },
];