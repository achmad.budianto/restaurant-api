import { Injectable, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { Repository, getRepository } from 'typeorm';
import { Menu } from './menu.entity';
import { CreateMenuDto } from './dto/create-menu.dto';
import { MenuRO } from './menu.interface';
import { validate } from 'class-validator';

@Injectable()
export class MenuService {

  constructor(
    @Inject('MENU_REPOSITORY')
    private menuRepository: Repository<Menu>,
  ) {}

  async findAll(): Promise<Menu[]> {
    return await this.menuRepository.find();
  }

  async create(dto: CreateMenuDto): Promise<MenuRO> {

    // check uniqueness of title
    const { title, description, price, image } = dto;
    const qb = await getRepository(Menu)
      .createQueryBuilder('menu')
      .where('menu.title = :title', { title });

    const menu = await qb.getOne();

    if (menu) {
      const errors = {title: 'Title menu must be unique.'};
      throw new HttpException({message: 'Input data validation failed', errors}, HttpStatus.BAD_REQUEST);
    }

    // create new menu
    let newMenu = new Menu();
    newMenu.title = title;
    newMenu.description = description;
    newMenu.price = price;
    newMenu.image = image;

    const errors = await validate(newMenu);
    if (errors.length > 0) {
      const _errors = {title: 'Userinput is not valid.'};
      throw new HttpException({message: 'Input data validation failed', _errors}, HttpStatus.BAD_REQUEST);

    } else {
      const savedMenu = await this.menuRepository.save(newMenu);
      return this.buildMenuRO(savedMenu);
    }
  }

  private buildMenuRO(menu: Menu) {
    const menuRO = {
      id: menu.id,
      title: menu.title,
      description: menu.description,
      price: menu.price,
      image: menu.image
    };

    return {menu: menuRO};
  }

}
