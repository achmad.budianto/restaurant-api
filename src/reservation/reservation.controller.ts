import { Controller, Get, Query, Post, Body } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ReservationService } from './reservation.service';
import { Reservation } from './reservation.entity';
import { CreateReservationDto } from './dto/create-reservation.dto';

@ApiBearerAuth()
@ApiTags('reservation')
@Controller('reservations')
export class ReservationController {
  constructor(private readonly reservationService: ReservationService) {}

  @ApiOperation({ summary: 'Get all reservations' })
  @ApiResponse({ status: 200, description: 'Return all reservations.'})
  @Get()
  async findAll(@Query() query): Promise<Reservation[]> {
    return await this.reservationService.findAll();
  }

  @ApiOperation({ summary: 'Create reservation' })
  @ApiResponse({ status: 201, description: 'New reservation has been successfully created.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @Post()
  async create(@Body() reservationData: CreateReservationDto) {
    return this.reservationService.create(reservationData);
  }
}
