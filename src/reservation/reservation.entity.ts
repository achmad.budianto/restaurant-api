import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('reservation')
export class Reservation {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  email: string;
  
  @Column()
  phone: string;

  @Column()
  persons: number;

  @Column()
  date: Date;

  @Column()
  time: string;

  @Column()
  message: string;
}
