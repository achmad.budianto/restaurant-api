export interface ReservationData {
  id: number;
  name: string;
  email: string;
  phone: string;
  persons: number;
  date: Date;
  message: string;
}

export interface ReservationRO {
  reservation: ReservationData;
}