import { Module } from '@nestjs/common';
import { ReservationService } from './reservation.service';
import { ReservationController } from './reservation.controller';
import { reservationProviders } from './reservation.providers';
import { DatabaseModule } from 'src/db/database.module';

@Module({
  imports: [DatabaseModule],
  providers: [
    ...reservationProviders,
    ReservationService],
  controllers: [ReservationController],
  exports: [ReservationService]
})
export class ReservationModule {}
