import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Reservation } from './reservation.entity';
import { CreateReservationDto } from './dto/create-reservation.dto';
import { ReservationRO } from './reservation.interface';

@Injectable()
export class ReservationService {

  constructor(
    @Inject('RESERVATION_REPOSITORY')
    private reservationRepository: Repository<Reservation>,
  ) {}

  async findAll(): Promise<Reservation[]> {
    return await this.reservationRepository.find();
  }

  async create(reservationData: CreateReservationDto): Promise<Reservation> {

    let reservation = new Reservation();
    reservation.name = reservationData.name;
    reservation.email = reservationData.email;
    reservation.phone = reservationData.phone;
    reservation.persons = reservationData.persons;
    reservation.date = reservationData.date;
    reservation.time = reservationData.time;
    reservation.message = reservationData.message;

    const newReservation = await this.reservationRepository.save(reservation);

    return newReservation;
  }
}
