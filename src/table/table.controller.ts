import { Controller, Get, Query } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Table } from './table.entity';
import { TableService } from './table.service';

@ApiBearerAuth()
@ApiTags('table')
@Controller('tables')
export class TableController {

  constructor(private readonly tableService: TableService) {}

  @ApiOperation({ summary: 'Get all tables' })
  @ApiResponse({ status: 200, description: 'Return all tables.'})
  @Get()
  async findAll(@Query() query): Promise<Table[]> {
    return await this.tableService.findAll();
  }
}
