import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany, OneToMany } from 'typeorm';

@Entity('table')
export class Table {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  no: string;

  @Column()
  seatNumbers: number;
}
