import { Module } from '@nestjs/common';
import { TableService } from './table.service';
import { TableController } from './table.controller';
import { tableProviders } from './table.providers';
import { DatabaseModule } from 'src/db/database.module';

@Module({
  imports: [DatabaseModule],
  providers: [
    ...tableProviders,
    TableService
  ],
  controllers: [TableController],
  exports: [TableService]
})
export class TableModule {}
