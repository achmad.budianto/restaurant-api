import { Connection } from 'typeorm';
import { Table } from './table.entity';

export const tableProviders = [
  {
    provide: 'TABLE_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Table),
    inject: ['DATABASE_CONNECTION'],
  },
];