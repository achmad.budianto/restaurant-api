import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Table } from './table.entity';

@Injectable()
export class TableService {
  constructor(
    @Inject('TABLE_REPOSITORY')
    private tableRepository: Repository<Table>,
  ) {}

  async findAll(): Promise<Table[]> {
    return await this.tableRepository.find();
  }
}
