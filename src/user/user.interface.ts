import { BaseRO } from "src/shared/base.interface";

export interface UserData {
  username: string;
  email: string;
  token: string;
  bio: string;
  image?: string;
}

export interface UserRO extends BaseRO {
  data: UserData;
}