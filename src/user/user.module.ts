import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { DatabaseModule } from 'src/db/database.module';
import { userProviders } from './user.providers';
import { AuthMiddleware } from 'src/shared/middleware/auth.middleware';

@Module({
  imports: [DatabaseModule],
  providers: [
    ...userProviders,
    UserService
  ],
  controllers: [UserController],
  exports: [UserService]
})
export class UserModule implements NestModule {

  public configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .forRoutes({path: 'user', method: RequestMethod.GET}, {path: 'user', method: RequestMethod.PUT});
  }
}
